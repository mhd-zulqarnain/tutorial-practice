import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by zulup on 4/25/2017.
 */
public class Iterables {

    public static void main(String[] args){
        Collection c1= Arrays.asList("red","orange","blue");
        Iterator iterator=c1.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }

    }
}
