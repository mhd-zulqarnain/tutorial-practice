import java.util.ArrayList;

/**
 * Created by zulup on 4/25/2017.
 */
public class ArrayListDemo {
    public static void main(String[] args){
        String[] plateform1={"PS4"};
        String[] plateform2={"Xbox","Wii U"};

        VedioPlayer v1=new VedioPlayer("Battlefield",2012,plateform1);
        VedioPlayer v2=new VedioPlayer("Prince of pesia",2001,plateform2);

        ArrayList<VedioPlayer> games=new ArrayList<VedioPlayer>();
        games.add(v1);
        games.add(v2);
        System.out.println(games);
    }
}
