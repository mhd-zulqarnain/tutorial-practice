/**
 * Created by zulup on 4/25/2017.
 */
public class VedioPlayer {

    private String name;
    private int year;
    private String[] plateform;
    public VedioPlayer(String name,int year,String[] plateform){
        this.name=name;
        this.year=year;
        this.plateform=plateform;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String[] getPlateform() {
        return plateform;
    }

    public void setPlateform(String[] plateform) {
        this.plateform = plateform;
    }
    @Override
    public String toString(){
        String  result="";

        result+="\nTitle:"+getName()+"\nYear:"+getYear()+"\nPlateform:";
        for(String plateform:getPlateform()){
            result+=plateform+" ";
        }
        return result;
    }
}
